import Vue from 'vue';
import Router from 'vue-router';

import Login from '../components/Auth/Login';
import Logout from '../components/Auth/Logout';
import Main from '../components/Main';

import Analysis from '../components/Stock/Analysis/Analysis';
// import UserInfo from '../components/UserInfo/UserInfo';
// import UserTradingJournal from '../components/UserInfo/UserTradingJournal';
// import UserSelectedStocks from '../components/UserInfo/UserSelectedStocks';
// import UserSettings from '../components/UserInfo/UserSettings';
// import Dashboard from '../components/Dashboard/Dashboard';

const UserInfo = () => import('../components/UserInfo/UserInfo.vue');
const UserTradingJournal = () => import('../components/UserInfo/UserTradingJournal.vue');
const UserSelectedStocks = () => import('../components/UserInfo/UserSelectedStocks.vue');
const UserSettings = () => import('../components/UserInfo/UserSettings.vue');
const Dashboard = () => import('../components/Dashboard/Dashboard.vue');

Vue.use(Router);

const routes = [
  { path: '/login', component: Login },
  { path: '/logout', component: Logout },
  { path: '/',
    component: Main,
    redirect: '/analysis',
    children: [
      { path: '/analysis', component: Analysis },
      { path: '/user',
        component: UserInfo,
        redirect: { name: 'trading-journal' },
        children: [
          { path: 'trading-journal',
            name: 'trading-journal',
            component: UserTradingJournal },
          { path: 'selected-stocks',
            name: 'selected-stocks',
            component: UserSelectedStocks },
          { path: 'settings',
            name: 'settings',
            component: UserSettings },
        ],
      },
      { path: '/dashboard', component: Dashboard },
    ],
  },
];

const router = new Router({
  mode: 'history',
  routes,
});

export default router;

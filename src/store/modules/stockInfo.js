/* eslint no-param-reassign: ["error", { "props": false }] */
/* eslint no-shadow: ["error", { "allow": ["state"] }] */

import { stockPrice } from '../../services/stockHelper';

const state = {
  stockLoading: false,
  stockDataSet: [],
};

const getters = {};

const actions = {
  getStockPrice(context, stockData) {
    context.commit('startStockLoading');
    stockPrice(stockData).then((res) => {
      context.commit('saveStockPrice', res);
      context.commit('endStockLoading');
    });
  },
};

const mutations = {
  startStockLoading(state) {
    state.stockLoading = true;
  },
  endStockLoading(state) {
    state.stockLoading = false;
  },
  saveStockPrice(state, data) {
    state.stockDataSet = data;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};

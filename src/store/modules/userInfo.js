/* eslint no-param-reassign: ["error", { "props": false }] */
/* eslint no-shadow: ["error", { "allow": ["state"] }] */
import * as userService from '../../services/userHelper';

const state = {
  selectedStocks: [],
};

const getters = {};

const actions = {
  getSelectedStocks(context, userName) {
    userService.userSelectedStocks(userName)
      .then((res) => {
        context.commit('userSelectedStocks', res);
      });
  },
};

const mutations = {
  userSelectedStocks(state, stocks) {
    state.selectedStocks = stocks;
  },
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};

import Vue from 'vue';
import Vuex from 'vuex';

import stockInfo from './modules/stockInfo';
import userInfo from './modules/userInfo';

Vue.use(Vuex);

export default new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  modules: {
    stockInfo,
    userInfo,
  },
});

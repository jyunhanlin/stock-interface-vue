import axios from 'axios';

function userSelectedStocks(userName) {
  return new Promise((resolve, reject) => {
    axios.get(`http://localhost:3001/user/selectedStocks/${userName}`)
      .then(res => resolve(res.data))
      .catch(err => reject(err));
  });
}

function addUserSelectedStocks(userName, stockNum) {
  return new Promise((resolve, reject) => {
    axios.post(`http://localhost:3001/user/selectedStocks/${userName}`, { number: stockNum })
      .then(res => resolve(res.data))
      .catch(err => reject(err));
  });
}

export { userSelectedStocks, addUserSelectedStocks };

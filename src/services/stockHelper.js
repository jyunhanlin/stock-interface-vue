import axios from 'axios';

function stockPrice(stockData) {
  // const url = `http://localhost:3001/stock/${stockData.type}/${stockData.number}/${stockData.fromDate}/${stockData.toDate}/price`;
  const url = `http://localhost:3001/stock/${stockData.type}/${stockData.number}/price?&from=${stockData.fromDate}&to=${stockData.toDate}`;
  return new Promise((resolve, reject) => {
    axios.get(url)
      .then(res => resolve(res.data))
      .catch(error => reject(error));
  });
}

function stockNum(queryText = '') {
  return new Promise((resolve, reject) => {
    axios.get(`http://localhost:3001/queryStockNum/file/${queryText}`)
      .then(res => resolve(res))
      .catch(err => reject(err));
  });
}

export { stockPrice, stockNum };
